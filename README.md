# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary
* Version
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* navigate in your terminal to your base directory where bin, include, lib, src,... are included
* create your virtualenv by typing:
	`virtualenv . -p python3`
* navigate into your src folder by:
	`cd src`
* inside you have to migrate the DB-structure by:
	`python manage.py migrate`
* and populate the DB with demo data:
	`python manage.py shell < demo-data/populate.py`
* start the server with:
	`python manage.py runserver`
